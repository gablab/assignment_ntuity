# NTUITY Example Project

This project is an assignment for a selection process; the requirements can be found in assignment.pdf and consist in the following user stories:
- [x] (US1) A user can view a list of locations, create new locations and update or delete existing ones.
- [x] (US2) A user can view a list of device models, create new device models and update or delete existing ones.
- [x] (US3) A user can specify for each device model which data points are supported (data points and their definition with name, unit, etc. can be hard-coded).
- [ ] (US4) A user can view a list of devices per location, create new devices for the respective location and update or delete existing ones.
  - partially complete: the update is missing
- [x] (US5) A user can assign a device model to a device when it is created.
- [x] (US6) A user can view details of a device, whereby all available and current data points are listed with their latest valid values.
- [x] (US7) A user can view the progression of a data point metric (specifically historical values of a "numerical" data point) in a chart view, whereby the chart is updated every 15 seconds.
  - The automatic reload regards the full page; this is not optimal, but the time to separate the plots in single pages was missing and no other solution was found.
- [x] (US8) A service is able to update any data point for a device via an API endpoint.

Correspondingly, tests should be created.

Voluntary tasks:
- [x] (T1) Simple OpenApi specification documentation;
- [ ] (T2) Securing the API with a shared secret;
- [ ] (T3) Integration of tests in a continuous integration pipeline.

## Setup
The project was generated following https://docs.docker.com/samples/rails/.
In order to run it, execute `docker-compose up` and open http://0.0.0.0:3000/. If the database is not present, create it with `docker-compose run web rake db:create` and run migrations with `docker-compose run web rails db:migrate`.

If a rebuild is needed, the following may cause trouble:
- previously running docker containers -> `docker rm $(docker -q -a)`;
- the `tmp` directory -> `sudo rm -rf tmp/`.

## Data model
For many-to-many relationships: http://joshfrankel.me/blog/create-a-many-to-many-activerecord-association-in-ruby-on-rails/.

## Adding data
For data types, access the console and execute a command like `DataPointType.new(name: "temperature",  unit: "K").save`.

For the JSON loading of new data points, see the scripts in `./example` and 
```
curl --location --request POST 'localhost:3000/devices/add_data_points' \
--header 'Content-Type: application/json' \
--data-raw '[{
    "device_name": "Batmobile",
    "data_point_type_name": "speed",
    "value": 17
},
{
    "device_name": "Batgun",
    "data_point_type_name": "charge",
    "value": 96
},
{
    "device_name": "Batgun",
    "data_point_type_name": "temperature",
    "value": 300
}
]'
```
