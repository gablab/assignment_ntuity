curl --request POST 'localhost:3000/devices/add_data_points' \
--header 'Content-Type: application/json' \
--data-raw "[{
    \"device_name\": \"Batmobile\",
    \"data_point_type_name\": \"speed\",
    \"value\": $1
},
{
    \"device_name\": \"Batgun\",
    \"data_point_type_name\": \"charge\",
    \"value\": $2
},
{
    \"device_name\": \"Batgun\",
    \"data_point_type_name\": \"temperature\",
    \"value\": $3
  }
]"
