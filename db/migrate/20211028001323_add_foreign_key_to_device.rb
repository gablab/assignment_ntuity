class AddForeignKeyToDevice < ActiveRecord::Migration[6.1]
  def change
    add_reference :devices, :device_model, null: false, foreign_key: true, on_delete: :cascade
  end
end
