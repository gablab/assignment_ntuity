class CreateDataPointTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :data_point_types do |t|
      t.string :name
      t.string :unit

      t.timestamps
    end
  end
end
