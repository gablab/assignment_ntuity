class AddForeignKeyToObservable < ActiveRecord::Migration[6.1]
  def change
    remove_reference :observables, :device_model
    add_reference :observables, :device_model, null: false, foreign_key: true, on_delete: :cascade
  end
end
