class AddModelToDevice < ActiveRecord::Migration[6.1]
  def change
    add_column :devices, :model, :string
  end
end
