class DeleteDeviceModelIdFromDevice < ActiveRecord::Migration[6.1]
  def change
    remove_column :devices, :device_model_id, :reference
  end
end
