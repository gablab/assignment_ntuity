class CreateObservables < ActiveRecord::Migration[6.1]
  def change
    create_table :observables do |t|
      t.references :data_point_type, null: false, foreign_key: true
      t.references :device_model, null: false, foreign_key: true

      t.timestamps
    end
  end
end
