class AddValueToDataPoint < ActiveRecord::Migration[6.1]
  def change
    add_column :data_points, :value, :decimal
  end
end
