class CreateDataPoints < ActiveRecord::Migration[6.1]
  def change
    create_table :data_points do |t|
      t.references :data_point_type, null: false, foreign_key: true
      t.references :device, null: false, foreign_key: true

      t.timestamps
    end
  end
end
