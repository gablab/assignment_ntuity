class CreateDataTypesRelation < ActiveRecord::Migration[6.1]
  def change
    create_table :devicemodels_datapointtypes do |t|
      t.belongs_to :device_models
      t.belongs_to :data_point_types
    end
  end
end
