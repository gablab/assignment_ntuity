class DeleteModelFromDevice < ActiveRecord::Migration[6.1]
  def change
    remove_column :devices, :model, :string
  end
end
