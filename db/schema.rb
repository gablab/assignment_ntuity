# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_10_28_002314) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "data_point_types", force: :cascade do |t|
    t.string "name"
    t.string "unit"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "data_points", force: :cascade do |t|
    t.bigint "data_point_type_id", null: false
    t.bigint "device_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.decimal "value"
    t.index ["data_point_type_id"], name: "index_data_points_on_data_point_type_id"
    t.index ["device_id"], name: "index_data_points_on_device_id"
  end

  create_table "device_models", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "devicemodels_datapointtypes", force: :cascade do |t|
    t.bigint "device_models_id"
    t.bigint "data_point_types_id"
    t.index ["data_point_types_id"], name: "index_devicemodels_datapointtypes_on_data_point_types_id"
    t.index ["device_models_id"], name: "index_devicemodels_datapointtypes_on_device_models_id"
  end

  create_table "devices", force: :cascade do |t|
    t.bigint "location_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "name"
    t.string "model"
    t.bigint "device_model_id", null: false
    t.index ["device_model_id"], name: "index_devices_on_device_model_id"
    t.index ["location_id"], name: "index_devices_on_location_id"
  end

  create_table "locations", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "observables", force: :cascade do |t|
    t.bigint "data_point_type_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "device_model_id", null: false
    t.index ["data_point_type_id"], name: "index_observables_on_data_point_type_id"
    t.index ["device_model_id"], name: "index_observables_on_device_model_id"
  end

  add_foreign_key "data_points", "data_point_types"
  add_foreign_key "data_points", "devices"
  add_foreign_key "devices", "device_models"
  add_foreign_key "devices", "locations"
  add_foreign_key "observables", "data_point_types"
  add_foreign_key "observables", "device_models"
end
