Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root "locations#index"

  resources :locations do 
    resources :devices
  end

  get "/devices/:id", to: "devices#show"
  post "/devices/add_data_points", to: "data_points#create_many"

  resources :device_models

  resources :data_point_types
end
