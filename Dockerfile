# syntax=docker/dockerfile:1
FROM ruby:3.0
RUN apt-get update -qq && apt-get install -y nodejs npm postgresql-client 
RUN npm install --global yarn
WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
RUN bundle install

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Install Google Chrome -> testing
#RUN apt-get update \
#    && wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb \
#    && apt-get install ./google-chrome*.deb --yes \
#    && rm ./google-chrome*.deb
#RUN apt-get install -y firefox-esr

# Configure the main process to run when running the image
CMD ["rails", "server", "-b", "0.0.0.0"]
