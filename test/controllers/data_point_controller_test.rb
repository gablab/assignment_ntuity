require "test_helper"

class DataPointControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get data_point_index_url
    assert_response :success
  end
end
