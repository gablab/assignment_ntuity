require "test_helper"

class LocationFlowTest < ActionDispatch::IntegrationTest
  test "can see the Locations page" do
    get "/"
    assert_select "h1", "Locations"
  end

  test "can create a new location" do
    get "/locations/new"
    assert_response :success

    post "/locations",
      params: { location: { name: "Parker's", description: "Spiderman's private house" } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_select "h1", "Parker's"
  end
end
