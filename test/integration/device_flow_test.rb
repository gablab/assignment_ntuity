require "test_helper"

class DeviceFlowTest < ActionDispatch::IntegrationTest
  test "can see the Locations page" do
    get "/"
    assert_select "h1", "Locations"
  end

  test "can create a new device" do
    post "/locations/#{locations(:location_1).id}/devices",
      params: { device: { name: "Superobject",
                          model: "#{device_models(:device_model_1).id}",
                          location: "#{locations(:location_1).id}" } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_select "h1", "#{locations(:location_1).name}"
  end
end
