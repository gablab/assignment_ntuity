require "test_helper"

class DeviceModelFlowTest < ActionDispatch::IntegrationTest
  test "can see the Device Model page" do
    get "/device_models"
    assert_select "h1", "Device Models"
  end

  test "can create a new device model" do
    get "/device_models/new"
    assert_response :success

    post "/device_models",
      params: { device_model: { name: "web_shooter",
                                data_point_type_ids: ["#{data_point_types(:data_point_type_1).id}"]} }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_select "h1", "web_shooter"
  end
end
