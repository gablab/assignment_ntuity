require "test_helper"

class DataPointsFlowTest < ActionDispatch::IntegrationTest

  test "can create a new data point" do
    post "/devices/add_data_points",
      params: { _json: [
        { device_name: "#{devices(:device_1).name}",
          data_point_type_name: "#{data_point_types(:data_point_type_1).name}",
          value: 3.14 },
        { device_name: "#{devices(:device_2).name}",
          data_point_type_name: "#{data_point_types(:data_point_type_2).name}",
          value: 3.14 }
    ]}
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_select "h1", "Locations"
  end
end
