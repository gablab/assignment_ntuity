class DataPointTypesController < ApplicationController
  def index
    @data_point_types = DataPointType.all
  end
  def show
    @data_point_type = DataPointType.find(params[:id])
  end
  def new
    @data_point_type = DataPointType.new
  end 
  def create
    @data_point_type = DataPointType.new(data_point_type_params)


    if @data_point_type.save
      redirect_to @data_point_type
    else
      render :new
    end
  end


  def edit
    @data_point_type = DataPointType.find(params[:id])
  end

  def update
    @data_point_type = DataPointType.find(params[:id])

    if @data_point_type.update(data_point_type_params)
      redirect_to @data_point_type
    else
      render :edit
    end
  end
    
  def destroy
    @data_point_type = DataPointType.find(params[:id])
    @data_point_type.destroy

    redirect_to root_path
  end

  
  private
    def data_point_type_params
      params.require(:data_point_type).permit(:name, :description)
    end
end
