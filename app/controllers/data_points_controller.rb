class DataPointsController < ApplicationController
  skip_before_action :verify_authenticity_token # XXX https://stackoverflow.com/q/23773730/5599687

  def index
  end
  
  def create_many
    params[:_json].each do |param_obj|
      @device = Device.find_by name: param_obj[:device_name]
      param_obj[:data_point_type_id] = (DataPointType.find_by name: param_obj[:data_point_type_name]).id
      @data_point = @device.data_points.create(device_id: @device.id,
                                               data_point_type_id: param_obj[:data_point_type_id],
                                               value: param_obj[:value]
                                              )
    end
    redirect_to locations_path
  end
  
end
