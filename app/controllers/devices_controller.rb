class DevicesController < ApplicationController
  def show
    @device = Device.find(params[:id])
  end
  def create
    @location = Location.find(params[:location_id])
    @device = @location.devices.create(name: params[:device][:name],
                                       model: params[:device][:model],
                                       device_model_id: DeviceModel.find(params[:device][:model]).id
                                      )
    redirect_to location_path(@location)
  end

  def destroy
    @location = Location.find(params[:location_id])
    @device = @location.devices.find(params[:id])
    @device.destroy
    redirect_to location_path(@location)
  end

  def edit
    @location = Location.find(params[:location_id])
    @device = @location.devices.find(params[:id])
  end

  def update
    @location = Location.find(params[:location_id])
    @device = @location.devices.find(params[:id])

    if @device.update(device_params)
      redirect_to location_path(@location)
    else
      render location_path(@location)
    end
  end
    
  

  private
    def device_params
      params.require(:device).permit(:name, :model)
    end

end
