class DeviceModelsController < ApplicationController
  def index
    @device_models = DeviceModel.all
  end
  def show
    @device_model = DeviceModel.find(params[:id])
  end
  def new
    @device_model = DeviceModel.new
  end 
  def create
    @device_model = DeviceModel.new(device_model_params)

    if @device_model.save
      params[:device_model][:data_point_type_ids].each do |data_point_type_id|
        if data_point_type_id != ""
        then @device_model.observables.create(data_point_type: DataPointType.find(data_point_type_id))
        end
      end
      redirect_to @device_model
    else
      render :new
    end
  end


  def edit
    @device_model = DeviceModel.find(params[:id])
  end

  def update
    @device_model = DeviceModel.find(params[:id])

    if @device_model.update(device_model_params)
      redirect_to @device_model
    else
      render :edit
    end
  end
    
  def destroy
    @device_model = DeviceModel.find(params[:id])
    @device_model.destroy

    redirect_to device_models_path
  end

  
  private
    def device_model_params
      params.require(:device_model).permit(:name, :description)
    end

end

