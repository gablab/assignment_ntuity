class Device < ApplicationRecord
  belongs_to :location
  has_many :data_points
end
