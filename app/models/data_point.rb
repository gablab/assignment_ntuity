class DataPoint < ApplicationRecord
  belongs_to :data_point_type
  belongs_to :device
end
