class Observable < ApplicationRecord
  belongs_to :data_point_type
  belongs_to :device_model
end
