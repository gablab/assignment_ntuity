class DeviceModel < ApplicationRecord
  has_many :observables
  has_many :data_point_types, through: :observables
end
