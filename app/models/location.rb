class Location < ApplicationRecord
  has_many :devices, dependent: :destroy

  validates :name, presence: true
  validates :description, presence: true
end
