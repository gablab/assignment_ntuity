class DataPointType < ApplicationRecord
  has_many :observables, dependent: :delete_all
  has_many :device_models, through: :observables, dependent: :delete_all
end
